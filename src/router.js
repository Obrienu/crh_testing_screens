import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import FeelookUp from "./pages/fee-look-up/feelookup";
import { AppStore } from "./app-context/AppContext";
import Verify from "./pages/verify/Verify";
import React, { useContext } from "react";
import Spinner from "./containers/Spinner/Spinner";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Routes = () => {
  const { loading } = useContext(AppStore);
  return (
    <Router>
        <Spinner loading={loading}/>
        <ToastContainer
          position="top-center"
          autoClose={5000}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss
          draggable
          pauseOnHover
        />
      <Switch>
        <Route exact path="/" component={FeelookUp} />
        <Route path="/verify" component={Verify} />
      </Switch>
    </Router>
  );
};

export default Routes;
