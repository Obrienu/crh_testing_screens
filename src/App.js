import './App.css';
import  Routes  from './router';
import AppContext from "./app-context/AppContext"

function App() {
  return (
    <div className="App">
      <AppContext>
        <Routes/>
      </AppContext>
    </div>
  );
}

export default App;
