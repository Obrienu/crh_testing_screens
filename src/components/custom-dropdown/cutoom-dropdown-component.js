import React from 'react';
import { v4 as uuidv4 } from 'uuid';

function CustomDropdown(props) {
    
    const {tag, defaultValue,fieldLabel, enumeratedValues, onChange } = props;
    const options = enumeratedValues.map(v => (<option value={v} key={uuidv4()}> {v} </option>))
    return (
        <div className="custom-input">
            <label className="label">
                {fieldLabel}
            </label>
            <select className="field" onChange={onChange} value={defaultValue} name={tag} >
                <option value=""> </option>
                {options}
            </select>
        </div>
    );
}

export default CustomDropdown;