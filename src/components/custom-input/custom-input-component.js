import React from 'react';
import './custom-input-styles.scss';


const getType = dataType => {
    switch (dataType.toLowerCase()) {
        case "number":
            return "number";
        case "date":
            return "date";
        default:
            return 'text';
    }
}

function CusomInput(props) {
     const {tag, defaultValue, validationRegEx, fieldMax,fieldMin, dataType, fieldLabel , onChange, disabled, required} = props;
     let type =  getType(dataType);
    return (
        <div className="custom-input">
            <label className="label"> 
                {fieldLabel} {required === 'REQ' && <span color="red">*</span>}
            </label>
            <input className="field"
                name={tag} 
                type={type} 
                value={defaultValue}
                pattern={validationRegEx}
                max={fieldMax}
                min={fieldMin}
                required = {required === 'REQ'}
                onChange = {onChange}
                disabled = {disabled}
                
            />
        </div>
    );
}

export default CusomInput;