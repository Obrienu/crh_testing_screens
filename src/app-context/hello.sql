PROCEDURE ADD_AFFILIATE_CREDENTIALS(
    resp_code OUT VARCHAR2,
    v_err_message OUT VARCHAR2,
    p_aff_code IN VARCHAR2,
    p_aff_name IN VARCHAR2,
    p_aff_curr IN VARCHAR2,
    p_aff_username IN VARCHAR2,
    p_aff_pass IN VARCHAR2,
    p_nostro_acc IN  VARCHAR2
    )
     IS
        resp_code VARCHAR2 (2);
        v_err_message VARCHAR2 (200);
        l_count NUMBER;
    BEGIN
        SELECT COUNT(1) INTO l_count FROM IMTO_AFFILIATE_CONFIG WHERE AFFILIATE_CODE = p_aff_code; 
        IF(l_count = 0) THEN
            resp_code := '00';
            INSERT INTO IMTO_AFFILIATE_CONFIG(AFFILIATE_CODE, AFFILIATE_NAME, CURRENCY, NOSTRO_ACCOUNT, USERNAME, PASSWORD)
            VALUES(p_aff_code, p_aff_name, p_aff_curr, p_nostro_acc ,p_aff_name, p_aff_pass );

             COMMIT;
             RETURN;
        ELSE
            resp_code := '00';
            UPDATE IMTO_AFFILIATE_CONFIG SET 
                AFFILIATE_CODE = p_aff_name, 
                AFFILIATE_NAME = p_aff_name,
                CURRENCY = p_aff_curr,
                NOSTRO_ACCOUNT = p_nostro_acc,
                USERNAME = p_aff_name,
                PASSWORD = p_aff_pass;
            COMMIT;
            RETURN;
        END IF;
    EXCEPTION
         WHEN OTHERS THEN
            ROLLBACK;
            v_err_message := sqlerrm;
            resp_code:='99';
        RETURN;
END ADD_AFFILIATE_CREDENTIALS;