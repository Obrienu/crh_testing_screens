import React, {useState, createContext} from 'react';
import { toast } from 'react-toastify';

export const AppStore = createContext()

function AppContext(props) {

    const [appState, setAppState] = useState({sessionId: "", gffp: [], error: {code: "", message: ""}, loading: false, transData: null, countryList: null});
    
    const setSessionId = (id) => setAppState({...appState, sessionId: id});
    const setGffp = (gffp, loading) => setAppState({...appState, gffp, loading});
    const setError = error => {
        setAppState(prevState => ({...prevState, error, loading: false}));
        toast.error(error.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            });
    }
    const setLoader = loading => setAppState({...appState,loading})
    const setTransData = transData => setAppState({...appState, transData});
    const setCountryList = (countryList, loading) => setAppState(prevState => ({...prevState, countryList, loading}));

    return (
        <AppStore.Provider value={{...appState , setSessionId, setGffp, setError, setLoader, setTransData, setCountryList}}>
            {props.children}
        </AppStore.Provider>
    );
}

export default AppContext;