import React from 'react';
import Loader from "react-loader-spinner";
import './Spinner-styles.scss';

function Spinner({loading}) {

    return (
        <div className={`spinner ${loading ? "show" : ""}`}>
            <Loader
                    type="Puff"
                    color="#00BFFF"
                    height={100}
                    width={100}
                />
        </div>
    );
}

export default Spinner;