import axios from 'axios';

const baseUrl = "http://10.4.139.49:7003";

const FeelookUpUrl = "http://localhost:8080/channelservices/lookup/fee";


const hostHeaderInfo = {
    "sourceCode": "SMRTLR",
    "affiliateCode": "ESN",
    "countryCode": "SN",
    "requestId": "SAYS019632909625",
    "requestToken": "XXXXXX",
    "sourceChannelId": "AGENT"
}


export const  feeLoookUpHostHeaderInfo = {
    ...hostHeaderInfo,
    requestType: "FEEINQ",
}

export const gffpHostHeader = {
    ...hostHeaderInfo,
    requestType: "gffpLookUp",
}

export const  countryListHostHeaderInfo = {
    ...hostHeaderInfo,
    requestType: "CountryLookUp"
}

const countryListUrl = "http://localhost:8085/imto/country/list"

export const fetchCountryList = async (data) => {
    try{
        console.log(data);
        const res = await axios.post(countryListUrl, data  );
        return res;
    }catch(error){
        console.error(error);
    }
}

export const feeCheck = async (data) => {
    try{
        const res = await axios.post(FeelookUpUrl, data,  );
        console.log(res);
        return res
    }catch(error){
        console.error(error);
    }
}

const gffpUrl = "http://localhost:8085/imto/gffp"

export const getGffp = async (data) => {
    try{
        const res = await axios.post(gffpUrl, data);
        return res;
    }catch(error){
        console.error(error);
    }
}

const testInput = [
    "Test Data 1", "Test Data 2", "Test Data 3", "Test Data 4", "Test Data 5"
]

export const cleanData = (data) => {

    let newData = [...data];

    newData.sort((a , b) => Number.parseInt(a.displayOrder ) 
                                - Number.parseInt(b.displayOrder ) );
    
    newData = newData.map(a => {
        if(a.enumerated){
            a.enumeratedValues = testInput;
            a.arrayLength = testInput.length
        }
        return a;
    })

    return newData;

}

