import React, { useState, useContext, useEffect } from "react";
import "./feelookup-styles.scss";
import CustomInput from "../../components/custom-input/custom-input-component";
import { feeLoookUpHostHeaderInfo, feeCheck, countryListHostHeaderInfo,
    fetchCountryList, } from "../../services/actions";
import { ListGroupItem, ListGroup, Button } from "reactstrap";
import { AppStore } from "../../app-context/AppContext";
import { v4 as uuidv4 } from "uuid";
import CustomSelect from "../../components/custom-dropdown/cutoom-dropdown-component";

export default function FeelookUp(props) {
  const {
    setError,
    setSessionId,
    setLoader,
    setTransData,
    setCountryList,
    countryList,
  } = useContext(AppStore);

  const handleFeeSelect = (data) => {
    setTransData(data);
    props.history.push("/verify");
  };

  const handleChange = (e) => {
    const {
      target: { value, name },
    } = e;

    setlookForm({ ...lookUpForm, [name]: value });

    if (name === "remittancePartner") {
      handleFetchCountryList(value);
    }
  };

  const handleCountryChange = (e) => {
    const {
      target: { value, name },
    } = e;
    let countryData = countryList.find((a) => a.countryName === value);
    if (countryData == null) return;
    if (name === "originatorCountry") {
      setlookForm((prevState) => ({
        ...prevState,
        countryIsoCodeO: countryData.countryCode,
        currencyIsoCodeO: countryData.receiveCurrency,
        originatorCountry: value,
      }));
    } else {
      setlookForm((prevState) => ({
        ...prevState,
        countryIsoCodeD: countryData.countryCode,
        currencyIsoCodeD: countryData.receiveCurrency,
        destinationCountry: value,
      }));
    }
  };

  const handleFetchCountryList = async (remittancePartner) => {
    await setCountryList(null, true);
    const res = await fetchCountryList({
      hostHeaderInfo: countryListHostHeaderInfo,
      remittancePartner,
    });
    if(res == null){
      setError({code: 500, message: "Connection Error"});
      return;
    }
    if (res.status !== 200) {
      setError(res.error);
      return;
    }
    if (res.data.hostHeader.responseCode !== "000") {
      setError({
        code: res.hostHeader.responseCode,
        message: res.hostHeader.responseMessage,
      });
    }
    setCountryList(res.data.countryISOList, false);
  };

  const [lookUpForm, setlookForm] = useState({
    remittancePartner: "",
    deliveryOption: "WILL_CALL",
    productType: "FEEINQ",
    amountType: "SEND_AMOUNT",
    countryIsoCodeO: "GMB",
    sendAmount: "",
    currencyIsoCodeO: "GMD",
    destinationCountry: "",
    countryIsoCodeD: "",
    currencyIsoCodeD: "",
    receiveAmount: "0",
  });

  const [feeLookUpData, setFeeLookUpData] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoader(true);
    setFeeLookUpData(null);
    const data = {
      hostHeaderInfo: feeLoookUpHostHeaderInfo,
      remittancePartner: lookUpForm.remittancePartner,
      deliveryOption: lookUpForm.deliveryOption,
      productType: lookUpForm.productType,
      amountType: lookUpForm.amountType,
      origination: {
        countryIsoCode: lookUpForm.countryIsoCodeO,
        sendAmount: lookUpForm.sendAmount,
        currencyIsoCode: lookUpForm.currencyIsoCodeO,
      },
      destination: {
        countryIsoCode: lookUpForm.countryIsoCodeD,
        receiveAmount: lookUpForm.receiveAmount,
        currencyIsoCode: lookUpForm.currencyIsoCodeD,
      },
    };
    const res = await feeCheck(data);

    if(res == null){
      setError({code: 500, message: "Connection Error"});
      return;
    }

    if (res.status !== 200) {
      setError(res.error);
      return;
    }

    if (res.data.hostHeaderInfo.responseCode !== "000") {
      setError({
        code: res.data.hostHeaderInfo.responseCode,
        message: res.data.hostHeaderInfo.responseMessage,
      });
      return;
    }

    setFeeLookUpData(res.data.feeInfo);
    setLoader(false);
    setSessionId(res.data.transactionSessionID);
  };

  return (
    <div className="fee-look-up">
      <form>
        <div className="grid">
          <CustomSelect
            tag="remittancePartner"
            defaultValue={lookUpForm.remittancePartner}
            fieldLabel="Remittance Partner"
            enumeratedValues={["MONEYGRAM", "WESTERNUNION"]}
            onChange={handleChange}
          />

          <CustomInput
            tag="deliveryOption"
            fieldLabel="Delivery Option"
            defaultValue={lookUpForm.deliveryOption}
            validationRegEx="\w*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={false}
          />
          <CustomInput
            tag="productType"
            fieldLabel="Product Type"
            defaultValue={lookUpForm.productType}
            validationRegEx="\w*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={false}
          />
          <CustomInput
            defaultValue={lookUpForm.amountType}
            fieldLabel="Amount Type"
            tag="amountType"
            validationRegEx="\w*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={false}
          />
          <CustomInput
            tag="sendAmount"
            fieldLabel="Send Amount"
            defaultValue={lookUpForm.sendAmount}
            validationRegEx=""
            fieldMax=""
            fieldMin=""
            dataType="number"
            onChange={handleChange}
            disabled={false}
          />
          <CustomInput
            tag="countryIsoCodeO"
            fieldLabel="Originator Country ISO Code"
            defaultValue={lookUpForm.countryIsoCodeO}
            validationRegEx="\w*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={false}
          />
          <CustomInput
            tag="currencyIsoCodeO"
            fieldLabel="Send Currency ISO"
            defaultValue={lookUpForm.currencyIsoCodeO}
            validationRegEx=""
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={false}
          />

          {countryList && (
            <>
              <CustomSelect
                tag="destinationCountry"
                defaultValue={lookUpForm.destinationCountry}
                fieldLabel="Destination Country"
                enumeratedValues={[
                  ...countryList
                    .filter((a) => a.receiveActive)
                    .map((a) => a.countryName),
                ]}
                onChange={handleCountryChange}
              />

              {
                <>
                  <CustomInput
                    tag="countryIsoCodeD"
                    fieldLabel="Destination Country ISO"
                    defaultValue={lookUpForm.countryIsoCodeD}
                    validationRegEx="\w*"
                    fieldMax=""
                    fieldMin=""
                    dataType="text"
                    onChange={handleChange}
                    disabled={true}
                  />
                  <CustomInput
                    tag="currencyIsoCodeD"
                    fieldLabel="Destination Currency ISO"
                    defaultValue={lookUpForm.currencyIsoCodeD}
                    validationRegEx="\w*"
                    fieldMax=""
                    fieldMin=""
                    dataType="text"
                    onChange={handleChange}
                    disabled={true}
                  />
                </>
              }
            </>
          )}
          <CustomInput
            tag="receiveAmount"
            fieldLabel="Receive Amount"
            defaultValue={lookUpForm.receiveAmount}
            validationRegEx="\w*"
            fieldMax=""
            fieldMin=""
            dataType="number"
            onChange={handleChange}
            disabled={false}
          />
        </div>

       { countryList && <button onClick={handleSubmit} className="my-custom-button">
          Submit
        </button>}
      </form>

      {feeLookUpData && (
        <ListGroup>
          {feeLookUpData.map((data) => (
            <ListGroupItem key={uuidv4()}>
              <div>
                Display Name : <em>{data.deliveryOptDisplayName}</em>
              </div>
              <div>
                Exchange Rate : <em>{data.exchangeRate}</em>
              </div>
              <div>
                Delivery Option : <em>{data.deliveryOption}</em>
              </div>
              <ListGroup>
                <h4>Destination</h4>
                <ListGroupItem>
                  <div>
                    Country Iso Code :{" "}
                    <em>{data.destination.countryIsoCode}</em>
                  </div>
                  <div>
                    Currency Iso Code :{" "}
                    <em>{data.destination.currencyIsoCode}</em>
                  </div>
                  <div>
                    Receive Amount : <em>{data.destination.receiveAmount}</em>
                  </div>
                </ListGroupItem>
              </ListGroup>
              <ListGroup>
                <h4>Origination</h4>
                <ListGroupItem>
                  <div>
                    Country Iso Code :{" "}
                    <em>{data.origination.countryIsoCode}</em>
                  </div>
                  <div>
                    Currency Iso Code :{" "}
                    <em>{data.origination.currencyIsoCode}</em>
                  </div>
                  <div>
                    Send Amount : <em>{data.origination.sendAmount}</em>
                  </div>
                  <div>
                    Total Charges : <em>{data.origination.totalCharges}</em>
                  </div>
                  <div>
                    Total Taxes : <em>{data.origination.totalTaxes}</em>
                  </div>
                </ListGroupItem>
              </ListGroup>
              <Button
                className="mt-3"
                color="primary"
                size="sm"
                onClick={() =>
                  handleFeeSelect({
                    ...data,
                    remittancePartner: lookUpForm.remittancePartner,
                  })
                }
              >
                Select
              </Button>
            </ListGroupItem>
          ))}
        </ListGroup>
      )}
    </div>
  );
}
