import React, { useState, useContext, useEffect } from "react";
import "./Verify-styles.scss";
import CustomInput from "../../components/custom-input/custom-input-component";
import CustomSelect from "../../components/custom-dropdown/cutoom-dropdown-component";
import { gffpHostHeader, getGffp, cleanData } from "../../services/actions";
import { mockGffp } from "../../services/mock-data";
import { AppStore } from "../../app-context/AppContext";
import { Card, CardBody, CardHeader } from "reactstrap";

function Verify(props) {
  const { setGffp, setError, setLoader, gffp, transData, sessionId } =
    useContext(AppStore);
  const [formData, setFormData] = useState({
    senderFirstName: "",
    senderLastName: "",
    senderCity: "",
    senderState: "",
    senderIdentityNumber: "",
    senderIdentityType: "PASSPORT",
    senderIdentityIssueDate: "09-JUL-2020",
    senderIdentityExpiryDate: "09-JUL-2020",
    senderNationality: "SN",
    senderAddressLine1: "Lagos Avenue",
    senderPostalCode: 23401,
    senderCountryIsoCode: "GMB",
    senderPhoneNumber: "5014544908",
    senderEmail: "daniellpollock@gmail.com",
    senderBankCode: "",
    senderAccountType: "",
    senderAccountNumber: "",
    receiverFirstName: "",
    receiverLastName: "",
    receiverCity: "",
    receiverState: "",
    receiverIdentityNumber: "",
    receiverIdentityType: "PASSPORT",
    receiverIdentityIssueDate: "09-JUL-2020",
    receiverIdentityExpiryDate: "09-JUL-2020",
    receiverNationality: "SN",
    receiverAddressLine1: "Lagos Avenue",
    receiverPostalCode: 23401,
    receiverCountryIsoCode: "GMB",
    receiverPhoneNumber: "5014544908",
    receiverEmail: "daniellpollock@gmail.com",
    receiverBankCode: "",
    receiverAccountType: "",
    receiverAccountNumber: "",
  });

  const [verifyFormDate, setVerifyFormData] = useState({
    receiveCountry: "",
    deliveryOption: "",
    thirdPartyType: "",
    receiveCurrency: "",
    amount: "",
    sendCurrency: "",
    productType: "",
    consumerId: "",
    remittancePartner: "",
  });

  useEffect(() => {
    if (!transData) {
      props.history.push("/");
    } else {
      setVerifyFormData({
        receiveCountry: transData.destination.countryIsoCode,
        deliveryOption: transData.deliveryOption,
        thirdPartyType: "",
        receiveCurrency: transData.destination.currencyIsoCode,
        amount: transData.origination.sendAmount,
        sendCurrency: transData.origination.currencyIsoCode,
        productType: "SEND",
        consumerId: "2345678",
        remittancePartner: transData.remittancePartner,
      });
    }
  }, [transData, props.history]);

  const handleChange = (e) => {
    e.preventDefault();
    const {
      target: { value, name },
    } = e;
    setVerifyFormData({ ...verifyFormDate, [name]: value });
  };

  const handleValidateChange = (e) => {
    e.preventDefault();
    const {
      target: { value, name },
    } = e;
    setFormData({ ...formData, [name]: value });
  };

  const submitVerifyForm = async (e) => {
    e.preventDefault();
    const data = {
      hostHeaderInfo: gffpHostHeader,
      ...verifyFormDate,
    };

    await setGffp([], true);
    const res = await getGffp(data);

    if (res.status !== 200) {
      setError({ code: res.status, message: res.error.message });
    }

    const response = res.data;

    if (response.hostHeaderInfo.responseCode !== "000") {
      setError({
        code: response.hostHeaderInfo.responseCode,
        message: response.hostHeaderInfo.responseMessage,
      });
      return;
    }

    if (!response.productFieldInfo) {
      response.productFieldInfo = mockGffp;
    }
    await setGffp(cleanData(response.productFieldInfo), false);
    filterAndSetFormData(response.productFieldInfo);
    // setGffp(cleanData(mockGffp));
    // filterAndSetFormData(mockGffp);
  };

  const filterAndSetFormData = (data) => {
    let obj = {};
    data.forEach((a) => {
      obj[a.tag] = a.defaultValue;
    });
    setFormData((prevState) => ({ ...prevState, ...obj }));
  };

  const verify = (e) => {
    e.preventDefault();
    console.log(formData);
  };

  return (
    <div className="verify-page">
      <form className="verify-form">
        <h4>
          Session ID: <em> {sessionId}</em>
        </h4>
        <div className="grid">
          <CustomInput
            tag="remittancePartner"
            fieldLabel="Remittance Partner"
            defaultValue={verifyFormDate.remittancePartner}
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={true}
          />
          <CustomInput
            tag="receiveCountry"
            fieldLabel="Receive Country"
            defaultValue={verifyFormDate.receiveCountry}
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={true}
          />
          <CustomInput
            tag="deliveryOption"
            fieldLabel="Delivery Option"
            defaultValue={verifyFormDate.deliveryOption}
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={true}
          />

          <CustomInput
            defaultValue={verifyFormDate.receiveCurrency}
            fieldLabel="Receive Currency"
            tag="receiveCurrency"
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={true}
          />
          <CustomInput
            tag="amount"
            fieldLabel="Amount"
            defaultValue={verifyFormDate.amount}
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={true}
          />
          <CustomInput
            tag="sendCurrency"
            fieldLabel="Send Currency"
            defaultValue={verifyFormDate.sendCurrency}
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
            disabled={true}
          />

          <CustomSelect
            tag="productType"
            defaultValue={verifyFormDate.productType}
            fieldLabel="Product Type"
            enumeratedValues={["SEND", "RECEIVE"]}
            onChange={handleChange}
          />

          <CustomInput
            tag="thirdPartyType"
            fieldLabel="Third Party Type"
            defaultValue={verifyFormDate.thirdPartyType}
            validationRegEx=".*"
            fieldMax=""
            fieldMin=""
            dataType="text"
            onChange={handleChange}
          />
        </div>
        <button onClick={submitVerifyForm} className="my-custom-button">
          Generate Form
        </button>
      </form>

      {gffp.length > 0 && (
        <form>
          <Card>
            <CardHeader>Sender Details</CardHeader>
            <CardBody>
              <div className="grid-three">
                <CustomInput
                  tag="senderFirstName"
                  fieldLabel="Senders Firstname"
                  defaultValue={formData.senderFirstName}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderFirstName"
                  fieldLabel="Senders Lastname"
                  defaultValue={formData.senderLastName}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderCity"
                  fieldLabel="Sender City"
                  defaultValue={formData.senderCity}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderState"
                  fieldLabel="Sender State"
                  defaultValue={formData.senderState}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderIdentityNumber"
                  fieldLabel="Sender Identity Number"
                  defaultValue={formData.senderIdentityNumber}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderIdentityType"
                  fieldLabel="Sender Identity Type"
                  defaultValue={formData.senderIdentityType}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderIdentityIssueDate"
                  fieldLabel="Sender Identity Issue Date"
                  defaultValue={formData.senderIdentityIssueDate}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="date"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderIdentityExpiryDate"
                  fieldLabel="Sender Identity Expiry Date"
                  defaultValue={formData.senderIdentityExpiryDate}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="date"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderNationality"
                  fieldLabel="Sender Nationality"
                  defaultValue={formData.senderNationality}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="senderAddressLine1"
                  fieldLabel="Sender AddressLine1"
                  defaultValue={formData.senderAddressLine1}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderPostalCode"
                  fieldLabel="Sender PostalCode"
                  defaultValue={formData.senderPostalCode}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderCountryIsoCode"
                  fieldLabel="Sender CountryIsoCode"
                  defaultValue={formData.senderCountryIsoCode}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="senderPhoneNumber"
                  fieldLabel="Sender Phone Number"
                  defaultValue={formData.senderPhoneNumber}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="senderEmail"
                  fieldLabel="Sender Email"
                  defaultValue={formData.senderEmail}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="senderBankCode"
                  fieldLabel="Sender BankCode"
                  defaultValue={formData.senderBankCode}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderAccountType"
                  fieldLabel="Sender AccountType"
                  defaultValue={formData.senderAccountType}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="senderAccountNumber"
                  fieldLabel="Sender AccountNumber"
                  defaultValue={formData.senderAccountNumber}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
              </div>
            </CardBody>
          </Card>

          <Card className="mt-3">
            <CardHeader>Receivers Details</CardHeader>
            <CardBody>
              <div className="grid-three">
                <CustomInput
                  tag="receiverFirstName"
                  fieldLabel="Receivers Firstname"
                  defaultValue={formData.receiverFirstName}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverFirstName"
                  fieldLabel="Receivers Lastname"
                  defaultValue={formData.receiverLastName}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverCity"
                  fieldLabel="Receivers City"
                  defaultValue={formData.receiverCity}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverState"
                  fieldLabel="Receivers State"
                  defaultValue={formData.receiverState}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverIdentityNumber"
                  fieldLabel="Receivers Identity Number"
                  defaultValue={formData.receiverIdentityNumber}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverIdentityType"
                  fieldLabel="Receivers Identity Type"
                  defaultValue={formData.receiverIdentityType}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverIdentityIssueDate"
                  fieldLabel="Receivers Identity Issue Date"
                  defaultValue={formData.receiverIdentityIssueDate}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="date"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverIdentityExpiryDate"
                  fieldLabel="Receivers Identity Expiry Date"
                  defaultValue={formData.receiverIdentityExpiryDate}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="date"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverNationality"
                  fieldLabel="Receivers Nationality"
                  defaultValue={formData.receiverNationality}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="receiverAddressLine1"
                  fieldLabel="Receivers AddressLine1"
                  defaultValue={formData.receiverAddressLine1}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverPostalCode"
                  fieldLabel="Receivers PostalCode"
                  defaultValue={formData.receiverPostalCode}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverCountryIsoCode"
                  fieldLabel="Receivers CountryIsoCode"
                  defaultValue={formData.receiverCountryIsoCode}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="receiverPhoneNumber"
                  fieldLabel="Receivers Phone Number"
                  defaultValue={formData.receiverPhoneNumber}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="receiverEmail"
                  fieldLabel="Receivers Email"
                  defaultValue={formData.receiverEmail}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
                <CustomInput
                  tag="receiverBankCode"
                  fieldLabel="Receivers BankCode"
                  defaultValue={formData.receiverBankCode}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverAccountType"
                  fieldLabel="Receivers AccountType"
                  defaultValue={formData.receiverAccountType}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />

                <CustomInput
                  tag="receiverAccountNumber"
                  fieldLabel="Receivers AccountNumber"
                  defaultValue={formData.receiverAccountNumber}
                  validationRegEx=".*"
                  fieldMax=""
                  fieldMin=""
                  dataType="text"
                  onChange={handleValidateChange}
                  disabled={false}
                />
              </div>
            </CardBody>
          </Card>

          <Card className="mt-3">
            <CardHeader>GFFP DYNAMIC DATA</CardHeader>
            <CardBody>
              <div className="grid-three">
                {gffp
                  
                  .map((input) =>
                    input.enumerated ? (
                      <CustomSelect
                        key={input.tag}
                        tag={input.tag}
                        defaultValue={formData[input.defaultValue]}
                        fieldLabel={input.fieldLabel}
                        enumeratedValues={input.enumeratedValues}
                        onChange={handleValidateChange}
                      />
                    ) : (
                      <CustomInput
                        key={input.tag}
                        tag={input.tag}
                        fieldLabel={input.fieldLabel}
                        defaultValue={formData[input.defaultValue]}
                        validationRegEx={
                          input.validationRegEx.length
                            ? input.validationRegEx
                            : ".*"
                        }
                        fieldMax={input.fieldMax}
                        fieldMin={input.fieldMin}
                        dataType="text"
                        onChange={handleValidateChange}
                        required={input.visibility}
                      />
                    )
                  )}
              </div>
            </CardBody>
          </Card>

          <button onClick={verify} className="my-custom-button">
            Submit
          </button>
        </form>
      )}
    </div>
  );
}

export default Verify;
